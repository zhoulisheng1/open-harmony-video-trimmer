
# ohos_video_trimmer

## 简介
ohos_video_trimmer是在OpenHarmony环境下，提供视频剪辑能力的三方库。


## 效果展示：
![gif](preview/preview.gif)
## 安装教程

```
 ohpm install @ohos/ohos_video_trimmer
```

OpenHarmony npm环境配置等更多内容，请参考 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md) 。

## 使用说明

### 使用VideoTrimmerView


1. 构建VideoTrimmerOption对象:

```
    getContext(this).resourceManager.getMediaContent($r('app.media.app_icon'))
      .then(uint8 =>{
        let imageSource = image.createImageSource(uint8.buffer as any); // 步骤一：文件转为pixelMap 然后变换 给Image组件
        imageSource.createPixelMap().then(pixelmap => {
          this.videoTrimmerOption = {
            srcFilePath: this.filePath,
            listener:{
              onStartTrim: ()=>{
                console.log('dodo  开始裁剪')
                this.dialogController.open()
              },
              onFinishTrim:(path:string) => {
                console.log('dodo  裁剪成功 path='+path)
                this.outPath = path;
                this.dialogController.close()
              },
              onCancel:()=>{
                console.log('dodo  用户取消')
                router.replaceUrl({url:'pages/Index',params:{outFile: this.outPath}})
              }
            },
            loadFrameListener:{
              onStartLoad:()=>{
                console.log('dodo  开始获取帧数据')
                this.dialogController.open()
              },
              onFinishLoad:()=>{
                console.log('dodo  获取帧数据结束')
                this.dialogController.close()
              }
            },
            frameBackground: "#FF669900",
            // @ts-ignore
            framePlaceholder: pixelmap
          }
        })


      })
```

2.  界面build()中使用VideoTrimmerView组件，传入VideoTrimmerOption对象




```
build() {
    Row() {
      Column() {
        VideoTrimmerView( {videoTrimmerOption:$videoTrimmerOption})
      }
      .width('100%')
    }
    .height('100%')
  }
 

```
## 接口说明
```
export class VideoTrimmerOption {
  srcFilePath:string; // 视频源路径
  listener:VideoTrimListener; // 裁剪回调
  loadFrameListener:VideoLoadFramesListener // 加载帧回调
  VIDEO_MAX_TIME?: number = 10; // 指定裁剪长度 默认值10秒
  VIDEO_MIN_TIME?: number = 3; // 最小剪辑时间3s
  MAX_COUNT_RANGE?: number = 10; //seekBar的区域内一共有多少张图片
  THUMB_WIDTH?: number = 30; // 裁剪视频预览长方形条状左右边缘宽度
  PAD_LINE_WIDTH?: number = 10;// 裁剪视频预览长方形条状上下边缘高度
  framePlaceholder?:PixelMap // 当加载帧没有完成，默认的占位图
  frameBackground?:string // 裁剪视频预览长方形条状区域背景颜色
}


```

## 约束与限制
在下述版本验证通过：

DevEco Studio: 4.0 Release (4.0.3.300), SDK: API10 (4.0.10.2)

## 目录结构

````

├─entry #demo
└─videotrimmer #库主体模块
    └─src
        └─main
            ├─ets
            │  └─components #库主要实现
            └─resources  #资源





````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/ohos_video_trimmer/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/ohos_video_trimmer/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/ohos_video_trimmer/blob/master/LICENSE) ，请自由地享受和参与开源。